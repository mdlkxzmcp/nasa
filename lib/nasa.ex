defmodule Nasa do
  @moduledoc """
  The most cutting edge algorithms, only at `Nasa`™.
  """

  @type mass :: pos_integer()
  @type directive :: :launch | :land
  @type gravity :: float()
  @type flight_route :: [{directive(), gravity()}]
  @type required_fuel :: non_neg_integer()

  @typep formula_function :: (mass() -> integer())

  @doc """
  Calculates required fuel to complete the `flight_route` based on `ship_mass`.

  ## Examples

      iex> ship_mass = 28801
      ...> flight_route = [{:launch, 9.807}, {:land, 1.62}, {:launch, 1.62}, {:land, 9.807}]
      ...> Nasa.calculate_required_fuel(ship_mass, flight_route)
      51898
  """
  @spec calculate_required_fuel(mass(), flight_route()) :: required_fuel()
  def calculate_required_fuel(ship_mass, flight_route) do
    flight_route
    |> Enum.reverse()
    |> Enum.reduce(0, fn route, fuel ->
      formula = get_formula_function(route)
      base_required_fuel = formula.(ship_mass + fuel)

      fuel + fuel_for_fuel(base_required_fuel, formula)
    end)
  end

  @spec get_formula_function({directive(), gravity()}) :: formula_function()
  defp get_formula_function({:launch, gravity}), do: &launch_fuel_formula(&1, gravity)
  defp get_formula_function({:land, gravity}), do: &landing_fuel_formula(&1, gravity)

  @spec launch_fuel_formula(mass(), gravity()) :: integer()
  defp launch_fuel_formula(mass, gravity),
    do: (mass * gravity * 0.042 - 33) |> floor()

  @spec landing_fuel_formula(mass(), gravity()) :: integer()
  defp landing_fuel_formula(mass, gravity),
    do: (mass * gravity * 0.033 - 42) |> floor()

  @spec fuel_for_fuel(mass(), formula_function(), required_fuel()) :: required_fuel()
  defp fuel_for_fuel(fuel, formula, required_fuel \\ 0)

  defp fuel_for_fuel(fuel, _formula, required_fuel) when fuel <= 0, do: required_fuel

  defp fuel_for_fuel(fuel, formula, required_fuel),
    do: fuel_for_fuel(formula.(fuel), formula, required_fuel + fuel)
end
