defmodule NasaTest do
  use ExUnit.Case
  doctest Nasa

  describe "calculate_required_fuel/2" do
    test "works for Mission on Mars" do
      weight = 14606
      flight_route = [{:launch, 9.807}, {:land, 3.711}, {:launch, 3.711}, {:land, 9.807}]

      required_fuel = Nasa.calculate_required_fuel(weight, flight_route)

      assert 33388 == required_fuel
    end

    test "works for Passenger ship" do
      weight = 75432

      flight_route = [
        {:launch, 9.807},
        {:land, 1.62},
        {:launch, 1.62},
        {:land, 3.711},
        {:launch, 3.711},
        {:land, 9.807}
      ]

      required_fuel = Nasa.calculate_required_fuel(weight, flight_route)

      assert 212_161 == required_fuel
    end

    test "works for single route" do
      weight = 28801
      flight_route = [{:land, 9.807}]

      required_fuel = Nasa.calculate_required_fuel(weight, flight_route)

      assert 13447 == required_fuel
    end
  end
end
